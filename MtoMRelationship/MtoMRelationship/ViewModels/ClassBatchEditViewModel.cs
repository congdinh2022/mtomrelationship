﻿using MtoMRelationship.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MtoMRelationship.ViewModels
{
    public class ClassBatchEditViewModel
    {

        public string ClassName { get; set; }

        public string ClassCode { get; set; }

        public ICollection<User> Managers { get; set; }

        public ICollection<string> SelectTrainerIds { get; set; }

        public ICollection<string> SelectManagerIds { get; set; }
    }
}
