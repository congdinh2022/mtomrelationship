﻿using System.Collections.Generic;

namespace MtoMRelationship.Data
{
    public class ClassBatch
    {
        public string Id { get; set; }

        public string ClassName { get; set; }

        public string ClassCode { get; set; }

        public ICollection<UserClassBatch> Managers { get; set; }
    }
}