﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace MtoMRelationship.Data
{
    public class User: IdentityUser
    {
        public ICollection<UserClassBatch> ClassBatches { get; set; }
    }
}