﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace MtoMRelationship.Data
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<ClassBatch> ClassBatches { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<UserClassBatch>().HasKey(sc => new { sc.UserId, sc.ClassBatchId });

            builder.Entity<UserClassBatch>()
                    .HasOne<User>(sc => sc.User)
                    .WithMany(s => s.ClassBatches)
                    .HasForeignKey(sc => sc.UserId);


            builder.Entity<UserClassBatch>()
                    .HasOne<ClassBatch>(sc => sc.ClassBatch)
                    .WithMany(s => s.Managers)
                    .HasForeignKey(sc => sc.ClassBatchId);
        }
    }
}
