﻿namespace MtoMRelationship.Data
{
    public class UserClassBatch
    {
        public string ClassBatchId { get; set; }

        public ClassBatch ClassBatch { get; set; }

        public string UserId { get; set; }

        public User User { get; set; }
    }
}