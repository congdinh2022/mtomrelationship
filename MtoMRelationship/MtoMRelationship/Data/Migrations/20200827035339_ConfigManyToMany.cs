﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MtoMRelationship.Data.Migrations
{
    public partial class ConfigManyToMany : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ClassBatches",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClassName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClassCode = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassBatches", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserClassBatch",
                columns: table => new
                {
                    ClassBatchId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserClassBatch", x => new { x.UserId, x.ClassBatchId });
                    table.ForeignKey(
                        name: "FK_UserClassBatch_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserClassBatch_ClassBatches_ClassBatchId",
                        column: x => x.ClassBatchId,
                        principalTable: "ClassBatches",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserClassBatch_ClassBatchId",
                table: "UserClassBatch",
                column: "ClassBatchId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserClassBatch");

            migrationBuilder.DropTable(
                name: "ClassBatches");
        }
    }
}
