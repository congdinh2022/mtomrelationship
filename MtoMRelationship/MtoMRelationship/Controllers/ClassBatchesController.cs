﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MtoMRelationship.Data;
using MtoMRelationship.ViewModels;

namespace MtoMRelationship.Controllers
{
    public class ClassBatchesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ClassBatchesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ClassBatches
        public async Task<IActionResult> Index()
        {
            return View(await _context.ClassBatches.ToListAsync());
        }

        // GET: ClassBatches/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var classBatch = await _context.ClassBatches
                .FirstOrDefaultAsync(m => m.Id == id);
            if (classBatch == null)
            {
                return NotFound();
            }

            return View(classBatch);
        }

        // GET: ClassBatches/Create
        public IActionResult Create()
        {
            ClassBatchEditViewModel classBatchEditViewModel = new ClassBatchEditViewModel
            {
                Managers = _context.Users.ToList(),
                SelectManagerIds = new List<string>()
            };
            return View(classBatchEditViewModel);
        }

        // POST: ClassBatches/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ClassBatchEditViewModel classBatchEditViewModel)
        {
            if (ModelState.IsValid)
            {
                var classBatch = new ClassBatch();

                var id = Guid.NewGuid().ToString();
                classBatch.Id = id;

                classBatch.ClassCode = classBatchEditViewModel.ClassCode;
                classBatch.ClassName = classBatchEditViewModel.ClassName;
                var userClassBatches = new List<UserClassBatch>();

                foreach (var item in classBatchEditViewModel.SelectManagerIds)
                {
                    var userClassBatch = new UserClassBatch()
                    {
                        ClassBatchId = id,
                        UserId = item
                    };
                    userClassBatches.Add(userClassBatch);
                }

                classBatch.Managers = userClassBatches;

                _context.ClassBatches.Add(classBatch);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(classBatchEditViewModel);
        }

        // GET: ClassBatches/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var classBatch = await _context.ClassBatches.FindAsync(id);
            if (classBatch == null)
            {
                return NotFound();
            }
            return View(classBatch);
        }

        // POST: ClassBatches/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,ClassName,ClassCode")] ClassBatch classBatch)
        {
            if (id != classBatch.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(classBatch);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ClassBatchExists(classBatch.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(classBatch);
        }

        // GET: ClassBatches/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var classBatch = await _context.ClassBatches
                .FirstOrDefaultAsync(m => m.Id == id);
            if (classBatch == null)
            {
                return NotFound();
            }

            return View(classBatch);
        }

        // POST: ClassBatches/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var classBatch = await _context.ClassBatches.FindAsync(id);
            _context.ClassBatches.Remove(classBatch);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ClassBatchExists(string id)
        {
            return _context.ClassBatches.Any(e => e.Id == id);
        }
    }
}
